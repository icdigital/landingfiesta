$(document).ready(function(){
	'use strict';
	
/*YOUTUBE*/
	//Load the IFrame Player API code asynchronously.
//    var tag = document.createElement('script');
//
//    tag.src = "https://www.youtube.com/iframe_api";
//    var firstScriptTag = document.getElementsByTagName('script')[0];
//    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
//	
//	function onYouTubePlayerAPIReady() {
//		var players = document.querySelectorAll(".youtube .player");
//		for (var i = 0; i < players.length; i++) {
//			new YT.Player(players[i], {
//				height: '244',
//				width: '434',
//				videoId: players[i].dataset.embed,
//				playerVars: {
//					'autoplay': 0,
//					'rel': 0,
//					'showinfo': 0
//				},
//				events: {
//					'onStateChange': onPlayerStateChange
//				}
//			});
//		}
//	}
//	
//	function onPlayerStateChange(event) {
//		if (event.data == YT.PlayerState.ENDED) {
//			$('.play-button').fadeIn('normal');
//		}
//	}
/*end*/

	
/*VIDEO COVER*/
$(".youtube .play-button").click(function(){
	
	var item_parent = $(this).parents(".youtube"),
		item_embed  = item_parent.data("embed"),
        item_player = item_parent.children(".play-button"),
		item_mask   = item_parent.children(".mask"),
		item_cover  = item_parent.children(".cover-video"),
		item_iframe = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+item_embed+'?rel=0&showinfo=0&autoplay=1" frameborder="0"></iframe>';

	if ($(this).parent().hasClass("cover-youtube")) {
		
		  $(".mask>span").animate({
		  	width: "120%",
		  	paddingTop: "120%"
		  }, {
			  duration: 400,
			  complete: function() {
				item_player.hide();
				item_mask.hide();
				item_cover.hide();
				item_parent.append(item_iframe);
			  }
		  });

	}else{
		
		item_player.hide();
		item_mask.hide();
		item_cover.hide();
		item_parent.append(item_iframe);
		
	}

});
/*END*/


/*YOUTUBE VIDEO*/
	var youtube = document.querySelectorAll(".youtube");
	for (var i = 0; i < youtube.length; i++) {
		var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/maxresdefault.jpg";
		var image = new Image();
		image.src = source;
		image.className = "img-responsive cover-video";
		image.addEventListener("load", function() {
			youtube[ i ].appendChild(image);
		}(i));
		/*youtube[i].addEventListener( "click", function() {
			var iframe = document.createElement( "iframe" );
			iframe.setAttribute( "frameborder", "0" );
			iframe.setAttribute( "allowfullscreen", "" );
			iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );
			
			this.innerHTML = "";
			this.appendChild(iframe);
		});*/	
	}
/*END*/


/*CAROUSEL*/
	//global variables
	var items = [];
	var startItem = 1;
	var position = 0;
	var itemCount = $(".carousel .item").length;
	var leftpos = itemCount;
	var resetCount = itemCount;
	
	//swap images function
	function swap(action) {
		var direction = action;
		
		//moving carousel backwards
		if(direction == 'counter-clockwise') {
			var leftitem = $('.left-pos').attr('id') - 1;
			if(leftitem == 0) {
				leftitem = itemCount;
			}
			
			$('.right-pos').removeClass('right-pos').addClass('back-pos');
			$('.active-pos').removeClass('active-pos').addClass('right-pos');
			$('.left-pos').removeClass('left-pos').addClass('active-pos');
			$('#'+leftitem+'').removeClass('back-pos').addClass('left-pos');
			
			startItem--;
			if(startItem < 1) {
				startItem = itemCount;
			}
		}
		
		//moving carousel forward
		if(direction == 'clockwise' || direction == '' || direction == null ) {
			function pos(positionvalue) {
				if(positionvalue != 'leftposition') {
					//increment image list id
					position++;
				
					//if final result is greater than image count, reset position.
					if((startItem+position) > resetCount) {
						position = 1-startItem;
					}
				}
			
				//setting the left positioned item
				if(positionvalue == 'leftposition') {
					//left positioned image should always be one left than main positioned image.
					position = startItem - 1;
					
					//reset last image in list to left position if first image is in main position
					if(position < 1) {
						position = itemCount;
					}
				}
			
				return position;
			}  
			
			$('#'+ startItem +'').removeClass('active-pos').addClass('left-pos');
			$('#'+ (startItem+pos()) +'').removeClass('right-pos').addClass('active-pos');
			$('#'+ (startItem+pos()) +'').removeClass('back-pos').addClass('right-pos');
			$('#'+ pos('leftposition') +'').removeClass('left-pos').addClass('back-pos');
			
			startItem++;
			position=0;
			if(startItem > itemCount) {
				startItem = 1;
			}
		}
	}
	
	//next button click function
	$(".next").click(function() {
		swap('clockwise');
	});
	
	//prev button click function
	$(".prev").click(function() {
		swap('counter-clockwise');
	});
	
	/*$(".item").click(function() {
		if($(this).attr('class') == 'items left-pos') {
			swap('counter-clockwise'); 
		}
		else {
			swap('clockwise'); 
		}
	});*/
/*END*/

/*OVERLAY FERRERO CODE*/
$("a.coockie, a.legal").click(function() {
	var classElm = $(this).attr("class");
	if ($(this).hasClass(classElm)) {
		//console.log(classElm);
		$('body').removeClass("overflowHidden");
		$('.overlayContent, .wrapContent').css({'display':'none'});
		
		$('body').addClass("overflowHidden");
		$('.overlayContent').css({
			'display':'block'
		});
		$('.wrapContent.'+classElm).css({
		   display:'block'
		});
	}
});

$('.overlayContent .wrap .closeOverlay').click(function() {
	$('body').removeClass("overflowHidden");
	$('.overlayContent').css({
		'display':'none'
	});
	$('.wrapContent').css({
	   display:'none'
	});
});
/*end*/
	
});